from django.urls import path
from .views import get_todolist, list_details, create_list, edit_list, delete_list, todo_item_create, edit_todo_item

urlpatterns = [
    path("", get_todolist, name="todo_list_list"),
    path("<int:id>/", list_details, name="todo_list_detail"),
    path("create/", create_list, name= "create_list"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todo_item, name="todo_item_update"),
]
