from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.
def get_todolist(request):
    lists = TodoList.objects.all()
    context={
        "todo_lists" : lists
    }
    return render(request, "todos/todos.html", context)

def list_details(request, id):
    one_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list" : one_list
    }
    # print(context)
    return render(request, "todos/one_list.html", context)

def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        form = TodoListForm()

    context = {
        "form" : form
    }

    return render(request, "todos/create.html", context)

def edit_list(request, id):
    todolist = get_object_or_404(TodoList, id = id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance = todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoListForm(instance = todolist)

    context = {
        "form" : form
    }
    return render(request,"todos/edit.html", context)

def delete_list(request, id):
    removedlist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        removedlist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        item = TodoItemForm(request.POST)
        if item.is_valid():
            new_item = item.save()
            return redirect("todo_list_detail", new_item.list.id)
    else:
        item = TodoItemForm()
    context = {
        "item" : item
    }
    return render(request, "todos/create_item.html", context)

def edit_todo_item(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance = todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoItemForm(instance = todoitem)

    context = {
        "form" : form
    }
    return render(request, "todos/edit_item.html", context)
