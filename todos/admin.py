from django.contrib import admin
from .models import TodoList, TodoItem

# Register your models here.
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ("name", "id")

    def __str__(self):
        return self.name

@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = ("task", "due_date", "is_completed")
